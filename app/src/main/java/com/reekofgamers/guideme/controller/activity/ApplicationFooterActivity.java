package com.reekofgamers.guideme.controller.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.POI;

import butterknife.OnClick;

public abstract class ApplicationFooterActivity extends AppCompatActivity {

    @OnClick(R.id.footer_guide_me__home)
    public void gotoHome() {startActivity(new Intent(this, MainActivity.class));}

    @OnClick(R.id.footer_guide_me__booth)
    public void gotoBooth() {
        startActivity(new Intent(this, BoothListActivity.class));
    }

    @OnClick(R.id.footer_guide_me__scan)
    public void gotoScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(true);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan Booth QR Code Here");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() != null) {
                POI target = new POI(result.getContents());
                Event event = ((GuideMe) getApplication()).getEvent();
                for(POI poi : event.getPois()) {
                    if(poi.equals(target)) {
                        Intent intent = new Intent(ApplicationFooterActivity.this, BoothDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("poi", new Gson().toJson(poi));
                        intent.putExtras(bundle);
                        startActivity(intent);
                        return;
                    }
                }
                Toast.makeText(this, "Unrecognizen QR Code", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.footer_guide_me__logout)
    public void gotoLogin() {
        ((GuideMe) getApplication()).setToken("");
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
