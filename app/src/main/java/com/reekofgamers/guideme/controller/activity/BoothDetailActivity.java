package com.reekofgamers.guideme.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.model.POI;
import com.reekofgamers.guideme.model.POIPicture;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vatsal.imagezoomer.ImageZoomButton;
import com.vatsal.imagezoomer.ZoomAnimation;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BoothDetailActivity extends AppCompatActivity {

    @BindView(R.id.booth_detail_act__toolbar) CollapsingToolbarLayout toolbar;
    @BindView(R.id.scrolling_content_act__recycler_view) RecyclerView horizontalListView;
    @BindView(R.id.scrolling_content_act__description) TextView description;
    @BindView(R.id.booth_detail_act__cover_image) ImageView coverImage;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;

    private POI thisPoi;

    private class ImageListAdapter extends RecyclerView.Adapter<ImageListViewHolder> {

        private ArrayList<POIPicture> boothImages;

        public ImageListAdapter(ArrayList<POIPicture> boothImages) {
            this.boothImages = boothImages;
        }

        @NonNull
        @Override
        public ImageListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            Context context = viewGroup.getContext();
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.row_horizontal_image_view, viewGroup, false);
            return new ImageListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ImageListViewHolder imageListViewHolder, int i) {
            Picasso.get().load(boothImages.get(i).getLink()).into(imageListViewHolder.imageView, new Callback() {
                @Override
                public void onSuccess() {
                    imageListViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ZoomAnimation zoomAnimation = new ZoomAnimation(BoothDetailActivity.this);
                            zoomAnimation.zoom(view, 1000);
                        }
                    });
                }

                @Override
                public void onError(Exception e) {
                }
            });
        }

        @Override
        public int getItemCount() {
            return boothImages.size();
        }
    }

    @OnClick(R.id.fab) void pointOnMap(){
        Intent intent = new Intent(this, MainActivity.class);

        Bundle extras = new Bundle();
        extras.putInt("focus_x", (int)thisPoi.getPositionX());
        extras.putInt("focus_y", (int)thisPoi.getPositionY());

        intent.putExtras(extras);

        startActivity(intent);
    }

    private class ImageListViewHolder extends RecyclerView.ViewHolder {

        private ImageButton imageView;

        public ImageListViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.horizontal_image_view__image);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booth_detail);
        ButterKnife.bind(this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        thisPoi = new Gson().fromJson(getIntent().getExtras().getString("poi"), POI.class);

        if(!(thisPoi.getCoverImage() == null)) {
            Picasso.get().load(thisPoi.getCoverImage()).into(coverImage);
        }

        toolbar.setTitle(thisPoi.getName());

        if(thisPoi.getPictures() == null) {
            horizontalListView.setVisibility(View.GONE);
        } else {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            horizontalListView.setAdapter(new ImageListAdapter(thisPoi.getPictures()));
            horizontalListView.setLayoutManager(layoutManager);
        }

        description.setText(thisPoi.getDescription());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
