package com.reekofgamers.guideme.controller.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.dao.UserDao;
import com.reekofgamers.guideme.model.ErrorResponseWrapper;
import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.LoginResponseWrapper;
import com.reekofgamers.guideme.model.User;
import com.reekofgamers.guideme.util.VolleyErrorListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_act__input_username) EditText username;
    @BindView(R.id.login_act__input_password) EditText password;

    private final int SIGNIN_REQUEST = 101;
    private ProgressDialog progressDialog;

    private Response.Listener<String> onLoginSuccessCallback = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.d(LoginActivity.class.getName(), response);
            LoginResponseWrapper loginResponse = new Gson().fromJson(response, LoginResponseWrapper.class);
            ((GuideMe) getApplication()).setToken(loginResponse.getToken());
            Intent intent = new Intent(LoginActivity.this, EventGateActivity.class);
            startActivity(intent);
            finish();
            progressDialog.dismiss();
        }
    };

    @OnClick(R.id.login_act__login_btn)
    void goToEventGate() {
//        Intent intent = new Intent(LoginActivity.this, EventGateActivity.class);
//        startActivity(intent);
//        finish();
        progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Logging in, please wait...");
        progressDialog.show();

        User user = new User(username.getText().toString(), password.getText().toString());
        new UserDao(this).get(user, onLoginSuccessCallback, new VolleyErrorListener(this, progressDialog));
    }

    @OnClick(R.id.login_act__signin_btn)
    void newUserLogin() {
        Intent intent = new Intent(this, SigninActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("username", username.getText().toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, SIGNIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGNIN_REQUEST && resultCode == Activity.RESULT_OK) {
            username.setText(data.getExtras().getString("username"));
            password.setText(data.getExtras().getString("password"));
            goToEventGate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }
}
