package com.reekofgamers.guideme.controller.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.dao.UserDao;
import com.reekofgamers.guideme.model.LoginResponseWrapper;
import com.reekofgamers.guideme.model.User;
import com.reekofgamers.guideme.util.VolleyErrorListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninActivity extends AppCompatActivity {

    @BindView(R.id.signin_act__input_username) TextView username;
    @BindView(R.id.signin_act__input_password) TextView password;
    @BindView(R.id.signin_act__input_password_confirm) TextView passwordConfirm;

    private ProgressDialog progressDialog;

    private Response.Listener<String> onLoginSuccessCallback = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.d(SigninActivity.class.getName(), response);
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("username", username.getText().toString());
            bundle.putString("password", password.getText().toString());
            intent.putExtras(bundle);
            setResult(Activity.RESULT_OK, intent);
            finish();
            progressDialog.dismiss();
        }
    };

    @OnClick(R.id.signin_act__login_btn)
    public void gotoLogin() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.signin_act__signin_btn)
    public void register() {
        if(isInputValid()) {
            progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creating account...");
            progressDialog.show();

            User user = new User(username.getText().toString(), password.getText().toString());
            new UserDao(this).save(user, onLoginSuccessCallback, new VolleyErrorListener(this, progressDialog));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);

        username.setText(getIntent().getExtras().getString("username"));
    }

    private boolean isInputValid() {
        if(!isUsernameValid()) {
            username.setError("Username too short");
            return false;
        } else {
            username.setError(null);
        }
        if(!isPasswordValid()) {
            password.setError("At least 6 characters long or does not match with confirm");
            return false;
        } else {
            password.setError(null);
        }

        return true;
    }

    private boolean isUsernameValid() {
        return username.getText().length() >= 4;
    }

    private boolean isPasswordValid() {
        return password.getText().length() >= 4 &&
                password.getText().toString().equals(passwordConfirm.getText().toString());
    }
}
