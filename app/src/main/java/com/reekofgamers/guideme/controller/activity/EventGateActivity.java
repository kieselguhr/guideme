package com.reekofgamers.guideme.controller.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.dao.EventDao;
import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.POI;
import com.reekofgamers.guideme.util.VolleyErrorListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventGateActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    private Response.Listener<String> onLoginSuccessCallback = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.d(EventGateActivity.class.getName(), response);
            Event event = new Gson().fromJson(response, Event.class);
            Log.d(EventGateActivity.class.getName(), new Gson().toJson(event));
            ((GuideMe) getApplication()).setEvent(event);

            Log.d("ASDF", event.getPois().get(0).getPictures().size() + " heyo" + event.getPois().toString());

            Intent intent = new Intent(EventGateActivity.this, MainActivity.class);
            startActivity(intent);
            progressDialog.dismiss();
            finish();
        }
    };

    @OnClick(R.id.event_gate_act__login_btn)
    public void gotoScan() {

//        Intent intent = new Intent(EventGateActivity.this, MainActivity.class);
//        startActivity(intent);
//        finish();
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(true);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan Booth QR Code Here");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
    }

    @OnClick(R.id.event_gate_act__logout_btn)
    public void logout() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void getIn(String code) {
        progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Getting into event...");
        progressDialog.show();

        POI poi = new POI(code);
        new EventDao(this).getByPOI(poi, onLoginSuccessCallback, new VolleyErrorListener(this, progressDialog));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
//            getIn("bbbd506d6ce4d6512478c189e62aeb2cb2f8554b741df");
//            return;
            if(result.getContents() != null) {
                getIn(result.getContents());
            } else {
                getIn("bbbd506d6ce4d6512478c189e62aeb2cb2f8554b741df");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_gate);
        ButterKnife.bind(this);
    }
}
