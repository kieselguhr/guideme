package com.reekofgamers.guideme.controller.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reekofgamers.guideme.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FooterFragment extends Fragment {

    public FooterFragment() {
    }

    @OnClick(R.id.footer_fragment__home)
    public void gotoHome() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_footer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

}

