package com.reekofgamers.guideme.controller.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.otaliastudios.zoom.ZoomEngine;
import com.otaliastudios.zoom.ZoomLayout;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.POI;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends ApplicationFooterActivity {

    @BindView(R.id.main_act__image_progress) ProgressBar progressBar;
    @BindView(R.id.main_act__zoom_image) ImageView zoomImageView;
    @BindView(R.id.main_act__zoom_view) ZoomLayout zoomLayout;
    @BindView(R.id.main_act__zoom_root) RelativeLayout zoomRoot;

    private Event event;

    @OnClick(R.id.main_act__search)
    public void gotoBoothListWithSearchOn() {
        startActivity(new Intent(this, BoothListActivity.class));
    }

    /**
    @OnClick(R.id.test_button)
    void testCamera(){

        float[] xs = new float[]{0.0f, 100.0f, 700.0f, 500.0f, 2000.0f };
        int counter = 0;

        Drawable d = zoomImageView.getDrawable();
        Log.d("ASDF", "width is " + d.getIntrinsicWidth() + " height "+ d.getIntrinsicHeight());

        ZoomEngine zoomEngine = zoomLayout.getEngine();
        zoomEngine.zoomTo(4.0f, true);

        Log.d("ASDF","Zoom is " + zoomEngine.getZoom());
        zoomEngine.panTo(-1 * xs[counter], -1 * (int) xs[counter], true);
//        zoomEngine.moveTo(-1 * xs[counter], -1 * (int) xs[counter], 3.0f, true);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) locationMarker.getLayoutParams();
        lp.setMargins((int) xs[counter], (int) xs[counter], 0, 0);

        locationMarker.setLayoutParams(lp);

        if(counter < xs.length-1){
            counter++;
        }else{
            counter = 0;
        }

        Log.d("ASDF", "X : " + zoomLayout.getPanX());
        Log.d("ASDF", "Y : " + zoomLayout.getPanY());

    }**/

    private void preparePOIS(){
        ArrayList<POI> pois = event.getPois();
        for (POI p: pois) {

             TextView textView = new TextView(MainActivity.this);
             textView.setText(p.getName());
             textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
             textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
             textView.setTextColor(Color.WHITE);

             textView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent = new Intent(MainActivity.this, BoothDetailActivity.class);
                     Bundle bundle = new Bundle();
                     bundle.putString("poi", new Gson().toJson(p));
                     intent.putExtras(bundle);
                     startActivity(intent);
                 }
             });

             RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
             RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
             relativeParams.setMargins((int)p.getPositionX(), (int)p.getPositionY(), 0, 0 );
             zoomRoot.addView(textView, relativeParams);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


//        zoomImageView = findViewById(R.id.main_act__zoom_image);
//        zoomImageView.setImageDrawable(getResources().getDrawable(R.drawable.placeholder_map));
//        zoomImageView.setVisibility(View.VISIBLE);

        event = ((GuideMe) getApplication()).getEvent();
        preparePOIS();
        Log.d(MainActivity.class.getName(), event.getMap());


//        Picasso.get().load(event.getMap()).into(new Target() {

//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
////                zoomImageView.setVisibility(View.VISIBLE);
//                progressBar.setVisibility(View.GONE);
//                zoomLayout.setBackground(new BitmapDrawable(getResources(), bitmap));
//            }
//
//            @Override
//            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//                e.printStackTrace();
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//            }
//        });

        Picasso.get().load(event.getMap()).into(zoomImageView, new Callback() {
            @Override
            public void onSuccess() {
                zoomImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

                ZoomEngine zoomEngine = zoomLayout.getEngine();

                Bundle extras = getIntent().getExtras();
                if(extras != null && extras.getInt("focus_x",0) !=0  && extras.getInt("focus_y",0) !=0){

                    int focusX = extras.getInt("focus_x",0);
                    int focusY = extras.getInt("focus_y",0);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("ASDF", "there is a parameter " + focusX);
//                            zoomEngine.panTo(-1 *focusX, -1 * focusY, true);
//                            zoomEngine.zoomTo(5.0f, true);
                            zoomEngine.moveTo(5.0f,-1 * focusX + 400, -1 * focusY + 500,  true);

                        }
                    }, 1000);

                }

            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }
        });

        ((ImageView) findViewById(R.id.footer_guide_me__home_ic)).
                setColorFilter(getResources().getColor(R.color.colorPrimary));
        ((TextView) findViewById(R.id.footer_guide_me__home_label)).
                setTextColor(getResources().getColor(R.color.colorPrimary));

    }
}
