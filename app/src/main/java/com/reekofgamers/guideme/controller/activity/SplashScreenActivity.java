package com.reekofgamers.guideme.controller.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.dao.UserDao;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        String token = ((GuideMe) getApplication()).getToken();

        if(!token.isEmpty()){
            new UserDao(this).checkToken(new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    proceed(new Runnable() {
                        @Override
                        public void run() {
                           Intent intent = new Intent(SplashScreenActivity.this, EventGateActivity.class);
                           startActivity(intent);
                           finish();
                        }
                    });
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ((GuideMe) getApplication()).setToken("");
                    proceed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            });

        }else{
            proceed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

    }

    private void proceed(Runnable runnable) {
        Handler handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }

}
