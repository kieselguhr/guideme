package com.reekofgamers.guideme.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.R;
import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.POI;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class BoothListActivity extends ApplicationFooterActivity {

    private class BoothListAdapter extends ArrayAdapter<POI> {

        private ArrayList<POI> pois;
        private class ViewHolder {
            public TextView name;
            public TextView description;
        }

        public BoothListAdapter(Context context, ArrayList<POI> pois) {
            super(context, 0, pois); // resource will be set later
            this.pois = pois;
        }

        @NonNull
        @Override
        public View getView(int position, View view, @NonNull ViewGroup parent) {
            POI poi = getItem(position);
            ViewHolder viewHolder;
            if(view == null) {
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(getContext()).inflate(R.layout.row_booth_list, parent, false);
                viewHolder.name = view.findViewById(R.id.booth_list_row__booth_name);
                viewHolder.description = view.findViewById(R.id.booth_list_row__booth_description);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            viewHolder.name.setText(poi.getName());
            viewHolder.description.setText(poi.getDescription());

            return view;
        }
    }

    private AdapterView.OnItemClickListener listViewItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent(BoothListActivity.this, BoothDetailActivity.class);
            Bundle bundle = new Bundle();
            POI target = ((GuideMe) BoothListActivity.this.getApplicationContext()).getEvent().getPois().get(i);
            bundle.putString("poi", new Gson().toJson(target));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    @BindView(R.id.main_act__list_view) ListView listView;

    private ArrayList<POI> pois;
    private ArrayList<POI> allPois;

    @BindView(R.id.main_act__search)
    EditText searchEdit;

    @OnTextChanged(R.id.main_act__search)
    public void onSearchQueryChanged(final CharSequence text) {
        pois.clear();
        for(int i = 0; i < allPois.size(); i++) {
            if (allPois.get(i).getName().toLowerCase().contains(text.toString().toLowerCase())) {
                pois.add(allPois.get(i));
            }
        }
//        pois.addAll(allPois);
//        pois.removeIf(poi -> !poi.getName().toLowerCase().contains(text.toString().toLowerCase()));
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booth_list);

        ButterKnife.bind(this);

        Event event = ((GuideMe) getApplication()).getEvent();
        pois = new ArrayList<>(event.getPois());
        allPois = new ArrayList<>(pois);

        listView.setAdapter(new BoothListAdapter(this, pois));
        listView.setOnItemClickListener(listViewItemClickListener);

        searchEdit.requestFocus();

        ((ImageView) findViewById(R.id.footer_guide_me__booth_ic)).
                setColorFilter(getResources().getColor(R.color.colorPrimary));
        ((TextView) findViewById(R.id.footer_guide_me__booth_label)).
                setTextColor(getResources().getColor(R.color.colorPrimary));
    }
}

