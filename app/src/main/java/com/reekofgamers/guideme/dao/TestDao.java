package com.reekofgamers.guideme.dao;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.reekofgamers.guideme.util.URL;

import java.util.HashMap;
import java.util.Map;

public class TestDao {

    private Context context;

    public TestDao(Context context) {
        this.context = context;
    }

    public boolean test() {
        Log.d("NOTICE ME", "begin");
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(Request.Method.GET, URL.service("events/1/"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("NOTICE ME", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("NOTICE ME", "Error");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                super.getHeaders();
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + "woi para wibu! dapet ga?");
                return header;
            }
        };
        queue.add(request);
        return true;
    }
}
