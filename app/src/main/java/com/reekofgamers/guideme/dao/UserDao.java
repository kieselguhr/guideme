package com.reekofgamers.guideme.dao;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.model.User;
import com.reekofgamers.guideme.util.URL;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserDao extends DAO<User> {

    public UserDao(Context context) {
        super(context);
    }

    public void checkToken(Response.Listener<String> successAction, Response.ErrorListener errorAction){
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(
                Request.Method.POST,
                URL.service("users/auth"),
                successAction,
                errorAction
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                super.getHeaders();
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + ((GuideMe) getContext().getApplicationContext()).getToken());
                return header;
            }
        };
        queue.add(request);
    }

    @Override
    public void get(User model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(
                Request.Method.POST,
                URL.service("users/sign_in"),
                successAction,
                errorAction
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                super.getHeaders();
                Map<String, String> header = new HashMap<>();
                header.put("Content-Type", "application/json; charset=utf-8");
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return new Gson().toJson(model).getBytes();
            }
        };
        queue.add(request);
    }

    @Override
    public void getAll(int refId, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }

    @Override
    public void save(User model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(
                Request.Method.POST,
                URL.service("users/sign_up"),
                successAction,
                errorAction
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                super.getHeaders();
                Map<String, String> header = new HashMap<>();
                header.put("Content-Type", "application/json; charset=utf-8");
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return new Gson().toJson(model).getBytes();
            }
        };
        queue.add(request);
    }

    @Override
    public void update(User model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }

    @Override
    public void delete(int id, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }
}
