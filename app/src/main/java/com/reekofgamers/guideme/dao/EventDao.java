package com.reekofgamers.guideme.dao;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.reekofgamers.guideme.GuideMe;
import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.POI;
import com.reekofgamers.guideme.util.URL;

import java.util.HashMap;
import java.util.Map;

public class EventDao extends DAO<Event> {

    public EventDao(Context context) {
        super(context);
    }

    @Override
    public void get(Event model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }

    public void getByPOI(POI model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(
                Request.Method.POST,
                URL.service("events/qr_login"),
                successAction,
                errorAction
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                super.getHeaders();
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + ((GuideMe) getContext().getApplicationContext()).getToken());
                header.put("Content-Type", "application/json; charset=utf-8");
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return new Gson().toJson(model).getBytes();
            }
        };
        queue.add(request);
    }

    @Override
    public void getAll(int refId, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }

    @Override
    public void save(Event model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }

    @Override
    public void update(Event model, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }

    @Override
    public void delete(int id, Response.Listener<String> successAction, Response.ErrorListener errorAction) {

    }
}
