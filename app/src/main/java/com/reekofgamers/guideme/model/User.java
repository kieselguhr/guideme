package com.reekofgamers.guideme.model;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class User {

    private int id;
    private String username;
    private String password;
    private ArrayList<POI> pois;

    public User() {
    }

    public User(String name, String password) {
        this.username = name;
        this.password = password;
    }

    public User(int id, String name, ArrayList<POI> pois) {
        this.id = id;
        this.username = name;
        this.pois = pois;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<POI> getPois() {
        return pois;
    }

    public void setPois(ArrayList<POI> pois) {
        this.pois = pois;
    }
}
