package com.reekofgamers.guideme.model;

public class POIPicture {

    private int id;
    private String link;
    private String name;

    public POIPicture() {
    }

    public POIPicture(int id, String link, String name) {
        this.id = id;
        this.link = link;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
