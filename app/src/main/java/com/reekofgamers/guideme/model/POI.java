package com.reekofgamers.guideme.model;

import java.util.ArrayList;

public class POI {

    private int id;
    private String name;
    private String description;
    private double positionx;
    private double positiony;
    private String cover_image;
    private String code;
    private ArrayList<POIPicture> pictures;
    private ArrayList<User> users;

    public POI() {
    }

    public POI(String code) {
        this.code = code;
    }

    public POI(int id, String name, String description, double positionX, double positionY, String coverImage, String code, ArrayList<POIPicture> pictures, ArrayList<User> users) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.positionx = positionX;
        this.positiony = positionY;
        this.cover_image = coverImage;
        this.code = code;
        this.pictures = pictures;
        this.users = users;
    }

    public POI(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof POI ? ((POI) obj).getCode().equals(code) : super.equals(obj);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPositionX() {
        return positionx;
    }

    public void setPositionX(double positionX) {
        this.positionx = positionX;
    }

    public double getPositionY() {
        return positiony;
    }

    public void setPositionY(double positionY) {
        this.positiony = positionY;
    }

    public String getCoverImage() {
        return cover_image;
    }

    public void setCoverImage(String coverImage) {
        this.cover_image = coverImage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<POIPicture> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<POIPicture> pictures) {
        this.pictures = pictures;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
}
