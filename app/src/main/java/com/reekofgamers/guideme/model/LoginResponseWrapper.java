package com.reekofgamers.guideme.model;

public class LoginResponseWrapper {

    private String token;
    private Object result;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
