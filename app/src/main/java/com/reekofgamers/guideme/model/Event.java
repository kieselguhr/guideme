package com.reekofgamers.guideme.model;

import java.util.ArrayList;

public class Event {

    private int id;
    private String map;
    private String name;
    private ArrayList<POI> pois;
    private ArrayList<User> users;

    public Event() {
    }

    public Event(int id, String map, ArrayList<POI> pois, ArrayList<User> users) {
        this.id = id;
        this.map = map;
        this.pois = pois;
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public ArrayList<POI> getPois() {
        return pois;
    }

    public void setPois(ArrayList<POI> pois) {
        this.pois = pois;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public String getEventName() {
        return name;
    }

    public void setEventName(String eventName) {
        this.name = eventName;
    }
}
