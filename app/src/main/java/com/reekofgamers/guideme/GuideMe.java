package com.reekofgamers.guideme;

import android.app.Application;
import android.content.SharedPreferences;

import com.reekofgamers.guideme.model.Event;
import com.reekofgamers.guideme.model.User;

public class GuideMe extends Application {

    private Event event;

    public String getToken() {
        SharedPreferences userSharedPreference = getSharedPreferences("user", MODE_PRIVATE);
        return userSharedPreference.getString("token", "");
    }

    public void setToken(String sessionToken) {
        SharedPreferences userSharedPreference = getSharedPreferences("user", MODE_PRIVATE);
        userSharedPreference.edit().putString("token", sessionToken).apply();
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
